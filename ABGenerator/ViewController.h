//
//  ViewController.h
//  ABGenerator
//
//  Created by Vladimir Kolbun on 4/26/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)generateAction:(id)sender;
- (IBAction)clearAction:(id)sender;

@property (weak, nonatomic) IBOutlet UISlider *generateCountSlider;
- (IBAction)generateCountChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *generateCountInfoLabel;

@end
