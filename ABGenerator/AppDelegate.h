//
//  AppDelegate.h
//  ABGenerator
//
//  Created by Vladimir Kolbun on 4/26/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
