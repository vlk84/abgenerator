//
//  ViewController.m
//  ABGenerator
//
//  Created by Vladimir Kolbun on 4/26/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "ViewController.h"
#import <AddressBook/AddressBook.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface ViewController ()
@property (nonatomic) int value;
@end

@implementation ViewController{
    SVProgressHUD *hud;
}

#pragma mark Actions

- (void) generateAB:(ABAddressBookRef)ab{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterSpellOutStyle];

    
    NSData *d1 = UIImageJPEGRepresentation([UIImage imageNamed:@"1.jpg"], 1);
    NSData *d2 = UIImageJPEGRepresentation([UIImage imageNamed:@"2.jpg"], 1);
    int count = (int)self.generateCountSlider.value;
    for (int i = 0; i < count; ++i){
        ABRecordRef person = ABPersonCreate();
        ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef)@"Test", NULL);
//        ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFStringRef)[formatter stringFromNumber:@(i)], NULL);
        ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFStringRef)[NSString  stringWithFormat:@"Test%04d",i], NULL);
        
        ABMutableMultiValueRef email = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(email, (__bridge CFTypeRef)([NSString stringWithFormat:@"generated%d@test.com", i]), CFSTR("email"), NULL);
        ABRecordSetValue(person, kABPersonEmailProperty, email, NULL);
        CFRelease(email);
        
        ABPersonSetImageData(person, (__bridge CFDataRef)(arc4random_uniform(2)?d1:d2), NULL);
        ABAddressBookAddRecord(ab, person, NULL);
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [SVProgressHUD showProgress:1.0*i/count
                                 status:[NSString stringWithFormat:@"Generated %d/%d", i+1, count]];
        });
        
    }
    dispatch_sync(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:@"Saving"];
    });
    ABAddressBookSave(ab, NULL);
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
}

- (IBAction)generateCountChanged:(UISlider*)sender {
    self.generateCountInfoLabel.text = [NSString stringWithFormat:@"Generate %d contacts", (int)self.generateCountSlider.value];
}

- (IBAction)generateAction:(id)sender {
    
    [SVProgressHUD show];
    if (&ABAddressBookRequestAccessWithCompletion){
        ABAddressBookRef ab = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookRequestAccessWithCompletion(ab, ^(bool granted, CFErrorRef error) {
            if (granted){
                [self generateAB:ab];
            }
        });
    }
    else{
        ABAddressBookRef ab = ABAddressBookCreate();
        [self generateAB:ab];
    }
    
}

- (IBAction)clearAction:(id)sender {
    [SVProgressHUD showWithStatus:@"Clearing addressbook"];
    
    ABAddressBookRef ab = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAddressBookRequestAccessWithCompletion(ab, ^(bool granted, CFErrorRef error) {
        CFArrayRef records = ABAddressBookCopyArrayOfAllPeople(ab);
        for (int i = 0; i < CFArrayGetCount(records); ++i){
            ABAddressBookRemoveRecord(ab, CFArrayGetValueAtIndex(records, i), NULL);
        }
        
        ABAddressBookSave(ab, NULL);
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
    
    
}

#pragma mark - Controller

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.generateCountInfoLabel.text = [NSString stringWithFormat:@"Generate %d contacts", (int)self.generateCountSlider.value];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setGenerateCountSlider:nil];
    [self setGenerateCountInfoLabel:nil];
    [super viewDidUnload];
}

@end
